#include "mazeloader.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

void MazeLoader::load(const QString &filepath, Maze &maze){
    QFile file(filepath);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return;
    }
    QTextStream in(&file);

    int type,w,h;
    in>>type;
    in>>w>>h;
    maze.resize(w,h);
    maze.setType(type);
    if(type==1){
        int x,y;
        in>>x>>y;
        maze.setGoal(x,y);
    } else {
        maze.setGoal(7,7);
    }
    for(int i=0;i<h;i++){
        for(int j=0;j<w;j++){
            char ch;
            CellData cell={{0},{0},{0},{0}};
            in>>ch;
            if(ch>='0'&&ch<='9'){
                cell.wall.half=ch-'0';
            } else if(ch>='a'&&ch<='f'){
                cell.wall.half=ch+0xa-'a';
            } else if(ch==' '||ch=='\n'||ch=='\r'){
                j--;
                continue;
            } else {
                QMessageBox mb;
                mb.setText("Invalid maze data");
                mb.exec();
                //std::cerr<<"Invalid maze data"<<std::endl;
                return;
            }
            maze.setCell((h-i-1)*w+j,cell);
        }
    }
    file.close();
}
