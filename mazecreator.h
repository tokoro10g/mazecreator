#ifndef MAZECREATOR_H
#define MAZECREATOR_H

#include <QGraphicsScene>
#include "maze.h"
#include "mazeloader.h"
#include "mazeexporter.h"
#include "mazepainter.h"

#include <stack>
#include <map>

class MazeCreator
{
public:
    MazeCreator();
    ~MazeCreator();

    QGraphicsScene *getScene() const { return scene; }
    Maze *getMaze() const { return maze; }

    const QString loadMaze();
    bool isLoaded() const{ return loaded; }
    bool isChanged() const{ return changed; }
    const QString saveMaze(QString savepath);
    const QString saveMazeAs();
    const QString saveMazeOver();

    void undo();
    void redo();

    void onClickMaze(const QPoint &point);
private:
    QGraphicsScene *scene;
    Maze *maze;
    MazePainter *mp;
    QString filepath;
    int w,h;
    int g_w,g_h;
    bool loaded;
    bool changed;
    std::stack< std::pair<Coord,Direction> > undoStack;
    std::stack< std::pair<Coord,Direction> > redoStack;
};

#endif // MAZECREATOR_H
