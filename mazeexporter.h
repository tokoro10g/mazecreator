#ifndef MAZEEXPORTER_H
#define MAZEEXPORTER_H

#include <QFile>

#include "maze.h"

class MazeExporter
{
private:
    MazeExporter();
public:
    static void save(const QString &_filepath,Maze &maze);
};

#endif // MAZEEXPORTER_H
