#pragma once

#include <vector>
#include <stdint.h>

typedef union{
	unsigned half:4;
	struct{
		unsigned NORTH:1;
		unsigned EAST:1;
		unsigned SOUTH:1;
		unsigned WEST:1;
	} bits;
} Direction;

typedef union{
	unsigned half:4;
	struct{
		unsigned ROUTE:1;
		unsigned START:1;
		unsigned CURRENT:1;
		unsigned END:1;
	} bits;
} CellFlag;

typedef union{
	unsigned half:4;
	struct{
		unsigned FLAG1:1;
		unsigned FLAG2:1;
		unsigned FLAG3:1;
		unsigned FLAG4:1;
	} bits;
} FlagSet;

typedef struct{
	Direction wall;
	CellFlag cflag;
	Direction chk_wall;
	FlagSet sflag;
} CellData;

typedef std::vector<CellData> MazeData;

struct Coord {
	int x;
	int y;
	Direction dir;
};

class Maze{
	private:
		MazeData data;
		int w;
		int h;
        int type;
        int gx,gy;
	public:
		static const Direction DirFront;
		static const Direction DirRight;
		static const Direction DirBack;
		static const Direction DirLeft;
	public:
        Maze():w(0),h(0),type(0),gx(0),gy(0){}
        Maze(int _w,int _h):w(_w),h(_h),type(0),gx(0),gy(0){}
        Maze(const Maze& m):data(m.data),w(m.w),h(m.h),type(m.type),gx(m.gx),gy(m.gy){}
		~Maze(){ data.clear(); }
		int getWidth() const{ return w; }
		int getHeight() const{ return h; }
        const CellData getCellData(const int x,const int y) const{ return data[y*w+x]; }
        const CellData getCellData(const Coord& c) const{ return data[c.y*w+c.x]; }
        const CellData getCellData(const int index) const{ return data[index]; }
        const MazeData& getMazeData() const{ return data; }
        int getType() const{ return type; }
        void setType(int _type){ type=_type; }
        int getGoalX() const{ return gx; }
        int getGoalY() const{ return gy; }
        int getGoalIndex() const { return gy*w+gx; }
        void setGoal(int _gx,int _gy){ gx=_gx;gy=_gy; }

		void resize(int _w,int _h);

		void addCell(CellData v);
        void setCell(int index,CellData v);

		void setWall(const Coord& c,Direction dir);
		void setChkWall(const Coord& c,Direction dir);
        void unsetWall(const Coord& c,Direction dir);
        void unsetChkWall(const Coord& c,Direction dir);
        void toggleWall(const Coord& c,Direction dir);
        void toggleChkWall(const Coord& c,Direction dir);
        bool isSetWall(const int index,Direction dir) const;
		bool isSetWall(const Coord& c,Direction dir) const;
		bool isSearchedWall(const Coord& c,Direction dir) const;
		bool isSearchedCell(const Coord& c) const;
		bool isSearchedCell(const int index) const;
};
