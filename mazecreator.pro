#-------------------------------------------------
#
# Project created by QtCreator 2014-10-06T00:58:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mazecreator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    myqgraphicsview.cpp \
    mazepainter.cpp \
    mazeloader.cpp \
    mazeexporter.cpp \
    mazecreator.cpp \
    maze.cpp

HEADERS  += mainwindow.h \
    myqgraphicsview.h \
    mazepainter.h \
    mazeloader.h \
    maze.h \
    mazeexporter.h \
    mazecreator.h

FORMS    += mainwindow.ui
