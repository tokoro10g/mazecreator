#ifndef MAZELOADER_H
#define MAZELOADER_H

#include <QFile>

#include "maze.h"

class MazeLoader
{
private:
    MazeLoader();
public:
    static void load(const QString &_filepath,Maze &maze);
};

#endif // MAZELOADER_H
