#include "mazeexporter.h"
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

void MazeExporter::save(const QString &filepath, Maze &maze){
    QFile file(filepath);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        return;
    }
    QTextStream out(&file);

    int w=maze.getWidth();
    int h=maze.getHeight();
    out<<maze.getType()<<endl;
    out<<w<<endl<<h<<endl;

	if(maze.getType()==1){
		out<<maze.getGoalX()<<endl;
		out<<maze.getGoalY()<<endl;
	}

    for(int i=h-1;i>=0;i--){
        for(int j=0;j<w;j++){
            char ch;
            CellData c=maze.getCellData(j,i);
            if(c.wall.half>=0x0&&c.wall.half<=0x9){
                ch=c.wall.half+'0';
            } else if(c.wall.half>=0xa&&c.wall.half<=0xf){
                ch=c.wall.half-0xa+'a';
            }
            out<<ch<<" ";
        }
        out<<endl;
    }
    file.close();
}
