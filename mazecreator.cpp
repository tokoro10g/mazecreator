#include "mazecreator.h"

#include <QFileDialog>
#include <QMessageBox>
#include <cmath>

MazeCreator::MazeCreator()
{
    scene=new QGraphicsScene();
    maze=new Maze();
    mp=new MazePainter(scene,maze);
}

MazeCreator::~MazeCreator(){
    delete mp;
    delete maze;
    delete scene;
}

const QString MazeCreator::loadMaze(){
    QString temp_filepath=QFileDialog::getOpenFileName(0,"Select Maze Data File","","Maze Data (*.dat)");
    if(temp_filepath.isNull()==false && temp_filepath.endsWith(".dat"))
    {
        filepath=temp_filepath;
        MazeLoader::load(filepath,*maze);
        mp->reloadMaze();
        mp->draw();
        w=maze->getWidth();
        h=maze->getHeight();
        g_w=mp->getWidth();
        g_h=mp->getHeight();
        loaded=true;
    }
    return filepath;
}

const QString MazeCreator::saveMaze(QString savepath){
    if(savepath.isNull()==false && savepath.endsWith(".dat")){
        filepath=savepath;
        MazeExporter::save(filepath,*maze);
        mp->draw();
        changed=false;
    }
    return filepath;
}

const QString MazeCreator::saveMazeAs(){
    QString savepath=QFileDialog::getSaveFileName(0,"Select File to Save","","Maze Data (*.dat)");
    return saveMaze(savepath);
}

const QString MazeCreator::saveMazeOver(){
    return saveMaze(filepath);
}

void MazeCreator::undo(){
    if(undoStack.empty()) return;
    std::pair<Coord,Direction> p=undoStack.top();
    Coord c=p.first;
    Direction dir=p.second;
    maze->toggleWall(c,dir);
    mp->draw();
    undoStack.pop();
    redoStack.push(p);
}

void MazeCreator::redo(){
    if(redoStack.empty()) return;
    std::pair<Coord,Direction> p=redoStack.top();
    maze->toggleWall(p.first,p.second);
    mp->draw();
    redoStack.pop();
    undoStack.push(p);
}

void MazeCreator::onClickMaze(const QPoint &point){
    if(!loaded) return;
    int mx=point.x()-(MazePainter::graphics_width-g_w)/2;
    int my=point.y()-(MazePainter::graphics_width-g_h)/2;

    int step=g_w/w;
    int x=mx/step;
    int y=my/step;

    if(x>w||y>h){
        return;
    }
    if(x==w){
        x=w-1;
    }
    if(y==h){
        y=h-1;
    }

    Direction dir;
    dir.half=0;
    if(hypot(mx-(2*x+1)*step/2,my-y*step)<step/3){
        // north
        dir=Maze::DirFront;
    } else if(hypot(mx-(x+1)*step,my-(2*y+1)*step/2)<step/3){
        // east
        dir=Maze::DirRight;
    } else if(hypot(mx-x*step,my-(2*y+1)*step/2)<step/3){
        // west
        dir=Maze::DirLeft;
    } else if(hypot(mx-(2*x+1)*step/2,my-(y+1)*step)<step/3){
        // south
        dir=Maze::DirBack;
    }
    if(dir.half){
        Coord c;
        c.x=x;
        c.y=h-y-1;
        maze->toggleWall(c,dir);
        changed=true;
        undoStack.push(std::make_pair(c,dir));
        while(!redoStack.empty()){
            redoStack.pop();
        }
        mp->draw();
    }

    QMessageBox msg;
    msg.setText(QString().sprintf("%d,%d,%d",x,y,dir.half));
    //msg.exec();
}
