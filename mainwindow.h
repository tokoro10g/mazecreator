#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "maze.h"
#include "mazepainter.h"
#include "mazeloader.h"

#include <QMainWindow>
#include <QGraphicsScene>

#include "mazecreator.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_graphicsView_pressed(const QPoint &point);

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

private:
    Ui::MainWindow *ui;
    MazeCreator creator;
};

#endif // MAINWINDOW_H
