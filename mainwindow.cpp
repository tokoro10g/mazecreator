#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(creator.getScene());
    creator.loadMaze();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_graphicsView_pressed(const QPoint &point)
{
    creator.onClickMaze(point);
}

void MainWindow::on_actionOpen_triggered()
{
    creator.loadMaze();
}

void MainWindow::on_actionSave_triggered()
{
    creator.saveMazeOver();
}

void MainWindow::on_actionSave_As_triggered()
{
    creator.saveMazeAs();
}

void MainWindow::on_actionUndo_triggered()
{
    creator.undo();
}

void MainWindow::on_actionRedo_triggered()
{
    creator.redo();
}
