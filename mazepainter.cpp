#include "mazepainter.h"

const int MazePainter::graphics_width=640;

MazePainter::MazePainter() : scene(0),maze(0)
{
}

MazePainter::MazePainter(QGraphicsScene *_scene,Maze *_maze) : scene(_scene),maze(_maze)
{
    reloadMaze();
}

MazePainter::~MazePainter(){
}

void MazePainter::reloadMaze(){
    int w=maze->getWidth();
    int h=maze->getHeight();
    if(w*h==0){
        step=0;
    } else {
        step=graphics_width/(w>h?w:h);
    }
}

void MazePainter::draw() const{
    if(scene==0||maze==0){
        return;
    }
    int w=maze->getWidth();
    int h=maze->getHeight();

    QPen pen;
    pen.setColor(Qt::gray);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(QColor(180,180,180));

    scene->clear();

    for(int i=0;i<=w;i++){
        if(i%5==4){
            scene->addRect(i*step,0,step,h*step,QPen(Qt::NoPen),brush);
        }
        scene->addLine(i*step,0,i*step,h*step,pen);
    }
    for(int i=0;i<=h;i++){
        if(i%5==w%5){
            scene->addRect(0,i*step,w*step,step,QPen(Qt::NoPen),brush);
        }
        scene->addLine(0,i*step,w*step,i*step,pen);
    }

    pen.setColor(Qt::black);
    pen.setWidth(2);
    for(int i=0;i<w;i++){
        for(int j=0;j<h;j++){
            if(i+1==w&&maze->isSetWall((h-j-1)*w+i,Maze::DirRight)){
                scene->addLine((i+1)*step,(j+1)*step,(i+1)*step,j*step,pen);
            }
            if(j+1==h&&maze->isSetWall((h-j-1)*w+i,Maze::DirBack)){
                scene->addLine(i*step,(j+1)*step,(i+1)*step,(j+1)*step,pen);
            }
            if(maze->isSetWall((h-j-1)*w+i,Maze::DirFront)){
                scene->addLine(i*step,j*step,(i+1)*step,j*step,pen);
            }
            if(maze->isSetWall((h-j-1)*w+i,Maze::DirLeft)){
                scene->addLine(i*step,(j+1)*step,i*step,j*step,pen);
            }
        }
    }
}
