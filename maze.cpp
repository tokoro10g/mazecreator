#include <iostream>
#include <vector>
#include "maze.h"

const Direction Maze::DirFront={0x1};
const Direction Maze::DirRight={0x2};
const Direction Maze::DirBack={0x4};
const Direction Maze::DirLeft={0x8};

void Maze::resize(int _w,int _h){
	w=_w; h=_h;
	data.resize(w*h);
}

void Maze::addCell(CellData v){
	data.push_back(v);
}

void Maze::setCell(int index,CellData v){
	data[index]=v;
}

void Maze::setWall(const Coord& c,Direction dir){
	data[c.y*w+c.x].wall.half|=dir.half;

    if(dir.bits.SOUTH&&c.y!=0){
        data[(c.y-1)*w+c.x].wall.bits.NORTH=1;
    }
    else if(dir.bits.EAST&&c.x!=w-1){
        data[c.y*w+c.x+1].wall.bits.WEST=1;
    }
    else if(dir.bits.NORTH&&c.y!=w-1){
        data[(c.y+1)*w+c.x].wall.bits.SOUTH=1;
    }
    else if(dir.bits.WEST&&c.x!=0){
        data[c.y*w+c.x-1].wall.bits.EAST=1;
    }
}

void Maze::setChkWall(const Coord& c,Direction dir){
	data[c.y*w+c.x].chk_wall.half|=dir.half;

    if(dir.bits.SOUTH&&c.y!=0){
        data[(c.y-1)*w+c.x].chk_wall.bits.NORTH=1;
    }
    else if(dir.bits.EAST&&c.x!=w-1){
        data[c.y*w+c.x+1].chk_wall.bits.WEST=1;
    }
    else if(dir.bits.NORTH&&c.y!=w-1){
        data[(c.y+1)*w+c.x].chk_wall.bits.SOUTH=1;
    }
    else if(dir.bits.WEST&&c.x!=0){
        data[c.y*w+c.x-1].chk_wall.bits.EAST=1;
    }
}

void Maze::unsetWall(const Coord& c,Direction dir){
    data[c.y*w+c.x].wall.half&=~(dir.half);

    if(dir.bits.SOUTH&&c.y!=0){
        data[(c.y-1)*w+c.x].wall.bits.NORTH=0;
    }
    else if(dir.bits.EAST&&c.x!=w-1){
        data[c.y*w+c.x+1].wall.bits.WEST=0;
    }
    else if(dir.bits.NORTH&&c.y!=w-1){
        data[(c.y+1)*w+c.x].wall.bits.SOUTH=0;
    }
    else if(dir.bits.WEST&&c.x!=0){
        data[c.y*w+c.x-1].wall.bits.EAST=0;
    }
}

void Maze::unsetChkWall(const Coord& c,Direction dir){
    data[c.y*w+c.x].chk_wall.half&=~(dir.half);

    if(dir.bits.SOUTH&&c.y!=0){
        data[(c.y-1)*w+c.x].chk_wall.bits.NORTH=0;
    }
    else if(dir.bits.EAST&&c.x!=w-1){
        data[c.y*w+c.x+1].chk_wall.bits.WEST=0;
    }
    else if(dir.bits.NORTH&&c.y!=w-1){
        data[(c.y+1)*w+c.x].chk_wall.bits.SOUTH=0;
    }
    else if(dir.bits.WEST&&c.x!=0){
        data[c.y*w+c.x-1].chk_wall.bits.EAST=0;
    }
}

void Maze::toggleWall(const Coord &c, Direction dir){
    if(isSetWall(c,dir)){
        unsetWall(c,dir);
    } else {
        setWall(c,dir);
    }
}

void Maze::toggleChkWall(const Coord &c, Direction dir){
    if(isSearchedWall(c,dir)){
        unsetChkWall(c,dir);
    } else {
        setChkWall(c,dir);
    }
}

bool Maze::isSetWall(const Coord& c,Direction dir) const{
	return data[c.y*w+c.x].wall.half&dir.half;
}

bool Maze::isSetWall(const int index,Direction dir) const{
    return data[index].wall.half&dir.half;
}

bool Maze::isSearchedWall(const Coord& c,Direction dir) const{
	return data[c.y*w+c.x].chk_wall.half&dir.half;
}
bool Maze::isSearchedCell(const Coord& c) const{
	return data[c.y*w+c.x].chk_wall.half==0xF;
}
bool Maze::isSearchedCell(const int index) const{
	return data[index].chk_wall.half==0xF;
}
